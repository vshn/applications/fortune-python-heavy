"""
Fortune Cookie Service
"""

import os
import math
import time
import subprocess
from flask import Flask, request, Response, jsonify
from flask.templating import render_template
from subprocess import run, PIPE
from random import randrange

app = Flask(__name__)  # Standard Flask app

version = '1.4-python-heavy'

# Increase for even slower responses, depending on your platform
seed = 50_000_000

# https://stackoverflow.com/a/62394945
hostname = subprocess.check_output('hostname').decode('utf8').replace('\n', '')

def calculate_fortune_number():
    """
    Performs an unreasonably long CPU-bound calculation, and returns the result and the time it took.
    """
    start = time.time()
    x = 0.0001
    for _ in range(0, randrange(seed)):
        x += math.sqrt(x)
    end = time.time()
    return x, end - start

def get_fortune_text():
    """
    Returns a fortune cookie from the underlying operating system.
    """
    fortune = run('fortune', stdout=PIPE, text=True).stdout
    return fortune

def respond_html(number, seconds, fortune):
    """
    Builds a response in HTML
    """
    html = render_template('fortune.html', number=number, message=fortune, version=version, seconds=seconds, hostname=hostname)
    resp = Response(html, mimetype='text/html')
    resp.headers['X-Fortune-Version'] = version
    resp.headers['X-Fortune-Time'] = seconds
    return resp

def respond_json(number, seconds, fortune):
    """
    Builds a response in JSON
    """
    resp = jsonify({ 'number': number, 'message': fortune, 'version': version, 'seconds': seconds, 'hostname': hostname })
    resp.headers['X-Fortune-Version'] = version
    resp.headers['X-Fortune-Time'] = seconds
    return resp

def respond_plain_text(number, seconds, fortune):
    """
    Builds a response in plain text
    """
    result = f"""Fortune Cookie of the Day {str(number)}

{fortune}
Version {version} – Executed in {seconds} seconds by {hostname}
"""
    resp = Response(result, mimetype='text/plain')
    resp.headers['X-Fortune-Version'] = version
    resp.headers['X-Fortune-Time'] = seconds
    return resp

@app.route("/healthz")
def alive():
    """
    Liveness probe for Kubernetes.
    """
    return "OK"

@app.route("/")
def fortune():
    """
    Print a random, hopefully interesting, adage, after some heavy calculation.
    """
    number, seconds = calculate_fortune_number()
    secs = f"{seconds:.2f}"
    fortune = get_fortune_text()
    accepts = request.headers['accept']
    if accepts == 'application/json':
        return respond_json(number, secs, fortune)

    if accepts == 'text/plain':
        return respond_plain_text(number, secs, fortune)

    # In all other cases, respond with HTML
    resp = respond_html(number, secs, fortune)
    return resp

if __name__ == "__main__":
    # Setting threaded to False and processes to 1, to make it single-threaded / single-process
    # https://stackoverflow.com/a/14823968
    app.run(threaded=False, processes=1, host='0.0.0.0', port=os.environ.get('listenport', 8080))
